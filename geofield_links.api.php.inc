<?php

/**
 * @file
 * Hooks provided by the Geofield Links module.
 */

/**
 * @defgroup geofield_links_api_hooks Geofield Links API Hooks
 * @{
 * There are several sets of hooks that get invoked to allow modules to add 
 * link formatters of their own:
 * - Creating or altering webservices:
 *   - hook_geofield_links_webservices_info()
 *   - hook_geofield_links_webservices_info_alter(&$webservices)
 * - Creating or altering formatters:
 *   - hook_geofield_links_formatters_info()
 *   - hook_geofield_links_formatters_info_alter(&$formatters)
 * - Creating or altering elements:
 *   - hook_geofield_links_elements_info()
 *   - hook_geofield_links_elements_info_alter(&$elements)
 * @}
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines the webservice which the links will be made for.
 *
 * @see geofield_links_get_geofield_links_webservices_info().
 *
 * @ingroup geofield_links_api_hooks
 */
function hook_geofield_links_webservices_info() {
  return array(
    'google_maps' => array(
      'title' => t('Google Maps'),
      'webservice_uri' => 'https://maps.google.com',
    ),
  );
}

/**
 * Modifies the webservices declared in hook_geofield_links_webservices_info().
 *
 * @param array $webservices 
 *   An array of webservice defenitions.
 * 
 * @see geofield_links_get_geofield_links_webservices_info()
 * @see hook_geofield_links_webservices_info()
 *
 * @ingroup geofield_links_api_hooks
 */
function hook_geofield_links_webservices_info_alter(&$webservices) {
  $webservices['google_maps'] = array(
      'title' => t('Google Maps'),
      'webservice_uri' => 'https://maps.google.com',
    );
}

/**
 * Defines the link formatters.
 *
 * @see geofield_links_get_geofield_links_formatters_info().
 *
 * @ingroup geofield_links_api_hooks
 */
function hook_geofield_links_formatters_info() {
  return array(
    'geofield_google_maps_links_basic_link' => array(
      'title' => t('Place on Google Maps'),
      'description' => t('Basic link to address on Google Maps'),
      'webservice' => 'google_maps',
      'callback' => 'geofield_google_maps_links_construct_basic_links',
      'default_settings' => array(
        'active' => 1,
        'title' => t('See on Google Maps'),
        'style' => 'basic_link',
        'options' => array(
          'attributes' => array(
            'target' => '_blank',
            'class' => '',
          ),
        ), 
      ),
    ), 
  );
}

/**
 * Modifies link formatters declared in hook_geofield_links_formatters_info().
 *
 * @param array $formatters 
 *   An array of link formatter defenitions.
 * 
 * @see geofield_links_get_geofield_links_formatters_info()
 * @see hook_geofield_links_formatters_info()
 *
 * @ingroup geofield_links_api_hooks
 */
function hook_geofield_links_formatters_info_alter(&$formatters) {
  $formatters['geofield_google_maps_links_basic_link'] = array(
    'title' => t('Place on Google Maps'),
    'description' => t('Basic link to address on Google Maps'),
    'webservice' => 'google_maps',
    'callback' => 'geofield_google_maps_links_construct_basic_links',
    'default_settings' => array(
      'active' => 1,
      'title' => t('See on Google Maps'),
      'style' => 'basic_link',
      'options' => array(
        'attributes' => array(
          'target' => '_blank',
          'class' => '',
        ),
      ), 
    ),
  );
}

/**
 * Defines the elements to be used to render the link markup.
 *
 * @see geofield_links_get_geofield_links_elements_info().
 *
 * @ingroup geofield_links_api_hooks
 */
function hook_geofield_links_elements_info() {
  return array(
    'basic_link' => array(
      'title' => t('Basic link'),
      'render_callback' => 'geofield_links_basic_link_element_render',
    ),
  );
}

/**
 * Modifies the markup elements declared in hook_geofield_links_elements_info().
 *
 * @param array $elements 
 *   An array of link markup elements defenitions.
 * 
 * @see geofield_links_get_geofield_links_elements_info()
 * @see hook_geofield_links_elements_info()
 *
 * @ingroup geofield_links_api_hooks
 */
function hook_geofield_links_elements_info_alter(&$elements) {
  $elements['basic_link'] = array(
      'title' => t('Basic link'),
      'render_callback' => 'geofield_links_basic_link_element_render',
    );
}

/**
 * @} End of "addtogroup hooks".
 */