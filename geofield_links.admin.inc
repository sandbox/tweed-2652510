<?php

/**
 * @file
 * Administrative page callbacks for the Geofield Links module.
 */

/**
 * Configuration form for Geofield Links settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function geofield_links_admin_settings_form(){

  $form = array();

  // Get the configured webservices settings.
  $webservices_settings = geofield_links_get_geofield_links_webservices_info();

  // Fieldset container for webservices settings.
  $form['geofield_links_webservices_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webservices'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE, 
  );
  
  foreach ($webservices_settings as $webservice_key => $webservice_info) {
    
    // Container to differentiate the webservices.
    $form['geofield_links_webservices_settings'][$webservice_key] = array(
      '#type' => 'container',
      '#tree' => TRUE, 
    );
    
    // The webservice uri.
    $form['geofield_links_webservices_settings'][$webservice_key]['webservice_uri'] = array(
      '#type' => 'textfield',
      '#title' => $webservice_info['title'],
      '#description' => t('Base webservice uri for the @title link.', array(
        '@title' => $webservice_info['title'],
        )),
      '#default_value' => $webservice_info['webservice_uri'],
    );
    
  }

  return system_settings_form($form);
}