<?php

/**
 * @file
 * Geofield link formatter hooks and helper functions.
 */

/**
 * Implements hook_field_formatter_info().
 */
function geofield_links_field_formatter_info() {
  
  $settings = array();
  $info = array();
  
  $links_formatters_info = geofield_links_get_geofield_links_formatters_info();
  
  foreach ($links_formatters_info as $link_key => $link_info) {
    $settings[$link_info['webservice']][$link_key] = $link_info['default_settings'];
  }
 
  $webservices_info = geofield_links_get_geofield_links_webservices_info();
  
  foreach ($webservices_info as $webservice_key => $webservice_info) {
    $info['geofield_link_formatter_' . $webservice_key] = array(
      'label' => t('Geofield Link (@webservice)', array(
        '@webservice' => $webservice_info['title'],
      )),
      'field types' => array('geofield'),
      'settings'  => $settings[$webservice_key],
    );
  }
  
  return $info;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function geofield_links_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
   
  //Initialize the element variable
  $element = array();
  
  // Get the stored settings.
  $display = $instance['display'][$view_mode];

  $links_formatters_info = geofield_links_get_geofield_links_formatters_info();
  
  foreach ($links_formatters_info as $link_key => $link_info) {
    
    // Settings for this formatter.
    $settings = $display['settings'][$link_key];
    
    $element[$link_key] = array(
      '#type' => 'fieldset',
      '#title' => $link_info['description'],
      '#tree' => TRUE, 
    );
    
    $element[$link_key]['active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Active'),
      '#description' => t('Activate the link?'),
      '#default_value' => $settings['active'],
    );
    
    $element[$link_key]['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Link title'),
      '#description' => t('Text that the link title will use.'),
      '#default_value' => $settings['title'],
    );
    
    $element[$link_key]['style'] = array(
      '#type' => 'select',
      '#title' => t('Output style'),
      '#description' => t('Select how the link should be shown'),
      '#default_value' => $settings['style'],
      '#options' => _geofield_links_get_geofield_links_elements(),
    );
    
    $element[$link_key]['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );
    
    $element[$link_key]['options']['attributes'] = array(
      '#type' => 'fieldset',
      '#title' => t('Attributes'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    
    $element[$link_key]['options']['attributes']['target'] = array(
      '#type' => 'select',
      '#title' => t('Link target'),
      '#description' => t('Specifies where to open the linked document'),
      '#default_value' => $settings['options']['attributes']['target'],
      '#options' => array(
        '_blank' => '_blank',
        '_parent' => '_parent',
        '_self' => '_self',
        '_top' => '_top',
      ),
    );
    
    $element[$link_key]['options']['attributes']['class'] = array(
      '#type' => 'textfield',
      '#title' => t('Classes'),
      '#description' => t('Add classes to the link (space separated)'),
      '#default_value' => $settings['options']['attributes']['class'],
    );
    
  }
  
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function geofield_links_field_formatter_settings_summary($field, $instance, $view_mode) {
  
  $summaries = array();
  
  // Get the stored settings.
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $links_formatters_info = geofield_links_get_geofield_links_formatters_info();
  
  foreach ($links_formatters_info as $link_key => $link_info) {
    
    $summaries[$link_key] = t('Link of type %link with title "%title" and output style %style is %active', array(
      '%link' => $link_info['title'],
      '%active' => $settings[$link_key]['active'] ? t('Active') : t('Not active'),
      '%title' => $settings[$link_key]['title'],
      '%style' => _geofield_links_get_geofield_links_elements($settings[$link_key]['style']),
    ));
        
  }
  
  $summary = implode('<br />', $summaries);
  
  return $summary;
}

/**
 * Implements hook_field_formatter_view().
 */
function geofield_links_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  
  // Initialize the elements
  $element = array(); 
  
  if (substr($display['type'], 0, strlen('geofield_link_formatter_')) === 'geofield_link_formatter_') {
    
    $links_formatters_info = geofield_links_get_geofield_links_formatters_info();
    $webservices_info = geofield_links_get_geofield_links_webservices_info();

    foreach ($links_formatters_info as $link_key => $link_info) {

      // Get the settings
      $settings = $display['settings'][$link_key]; 

      if ($settings['active'] == 1) {

        $formatter_callback = $link_info['callback'];

        if (function_exists($formatter_callback)) {

          $settings['webservice_uri'] = $webservices_info[$link_info['webservice']]['webservice_uri'];

          $links = call_user_func_array($formatter_callback, array($settings, $items));

          $element_info = geofield_links_get_geofield_links_elements_info($settings['style']);

          $element_callback = $element_info['callback'];

          if (function_exists($element_callback)) {
            foreach ($links as $delta => $link) {
              $element[][$link_key][$delta] = call_user_func_array($element_callback, array($link)); 
            }
          } 
        }
      }
    }
  }

  return $element;
}

/**
 * Get defined Geofield Links link formatters.
 * 
 * @param string $key 
 *   If provided will filter return value with the key.
 * 
 * @return array
 *   An array of link formatters definitions or a link formatter definition if
 *   a key is provided.
 */
function geofield_links_get_geofield_links_formatters_info($key = '') {
  
  // Get defined formatters from modules implementing hook_geofield_links_formatters_info().
  $formatters = module_invoke_all('geofield_links_formatters_info');
  
  // Allow formatters to be altered.
  drupal_alter('geofield_links_formatters_info', $formatters);
  
  return empty($key) ? $formatters : $formatters[$key];
}

/**
 * Get defined Geofield Links elements to be used to render the link.
 * 
 * @param string $key 
 *   If provided will filter return value with the key.
 * 
 * @return array
 *   An array of link element definitions or a link element definition if
 *   a key is provided.
 */
function geofield_links_get_geofield_links_elements_info($key = '') {
   
  // Get defined elements from modules implementing hook_geofield_links_elements_info().
  $elements = module_invoke_all('geofield_links_elements_info');
  
  // Allow elements to be altered.
  drupal_alter('geofield_links_elements_info', $elements);
  
  return empty($key) ? $elements : $elements[$key];
}

/**
 * Get configured webservices which the links will be made for.
 * 
 * @param string $key 
 *   If provided will filter return value with the key.
 * 
 * @return array
 *   An array of configured webservice definitions or a webservice definition if
 *   a key is provided.
 */
function geofield_links_get_geofield_links_webservices_info($key = '') {
   
  // Get defined webservices from modules implementing hook_geofield_links_webservices_info().
  $webservices = module_invoke_all('geofield_links_webservices_info');
  
  // Allow webservices to be altered.
  drupal_alter('geofield_links_webservices_info', $webservices);
  
  // Get the configured webservice settings.
  $webservices_settings = variable_get('geofield_links_webservices_settings', array());
  
  // Merge the webservices with configured webservice settings.
  $webservices += $webservices_settings;
  
  return empty($key) ? $webservices : $webservices[$key];
}

/**
 * Implements hook_geofield_links_elements_info().
 */
function geofield_links_geofield_links_elements_info() {
  return array(
    'basic_link' => array(
      'title' => t('Basic link'),
      'callback' => 'geofield_links_basic_link_element_render',
    ),
    'button' => array(
      'title' => t('Button'),
      'callback' => 'geofield_links_button_element_render',
    ),
  );
}

/**
 * Gets a list of defined titles of Geofield Links elements keyed by their key.
 * 
 * @param string $key
 *   If provided will filter return value with the key.
 * @return array|string
 *   An array of link elements defined as $key => $title.
 */
function _geofield_links_get_geofield_links_elements($key = '') {
  
  $elements = array();
  
  $elements_info = geofield_links_get_geofield_links_elements_info();
  
  foreach ($elements_info as $element_key => $element) {
    $elements[$element_key] = $element['title'];
  }
   
  return empty($key) ? $elements : $elements[$key];
}

/**
 * Render callback for the basic link element.
 * 
 * @param array $link
 *   The configured link elements (text, path, options)
 * @return array
 *   A render array containing the link as a basic link.
 */
function geofield_links_basic_link_element_render($link) {
  
  $output = array(
    '#markup' => l($link['text'], $link['path'], $link['options']),
  ); 
  
  return $output;
}

/**
 * Render callback for the button element.
 * 
 * @param array $link
 *   The configured link elements (text, path, options)
 * @return array
 *   A render array containing the link as a button.
 */
function geofield_links_button_element_render($link) {
  
  $link['options']['html'] = TRUE; 
  
  $output = array(
    '#markup' => l('<button>' . $link['text'] . '</button>', $link['path'], $link['options']),
  ); 
  
  return $output;
}